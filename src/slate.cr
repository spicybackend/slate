# TODO: Write documentation for `Slate`
require "yaml"

module Slate
  VERSION = "0.1.0"
  DEFAULT_FIXTURE_PATH = "spec/fixtures"
  FIXTURE_EXTENSION = ".yml"

  def self.load_fixtures
    # scan "spec/fixtures" for yml files -- or path from config
    fixture_path = DEFAULT_FIXTURE_PATH

    fixture_entries = Dir.entries(fixture_path)
    fixture_entry_paths = fixture_entries.map { |entry| "#{fixture_path}/#{entry}" }
    fixture_files = fixture_entry_paths.select { |file_path| file_path.ends_with?(FIXTURE_EXTENSION) }

    load_fixtures(fixture_files)
  end

  def self.load_fixtures(fixture_paths : Array(String))
    fixture_paths.each do |fixture_path|
      load_fixture(fixture_path)
    end
  end

  private def self.load_fixture(fixture_path)
    table_name = if table_name_match_data = fixture_path.match(/^.*\/(.*).yml$/)
      table_name_match_data[1]
    else
      nil
    end

    parsed = YAML.parse(File.read(fixture_path)).as_h

    return unless parsed.size > 0

    columns = "(#{parsed.values.first.as_h.keys.join(",")})"

    data_inserts = parsed.map do |fixture_name, fixture_data|
      fixture_values = fixture_data.as_h.values
    end

    data_insert_bracks = data_inserts.map_with_index do |insert, fix_num|
      holders = [] of String

      insert.size.times do |i|
        offset = fix_num * insert.size
        holder = "$#{i + 1 + offset}"

        holders << holder
      end

      "(#{holders.join(",")})"
    end

    query = "INSERT INTO #{table_name} #{columns} VALUES #{data_insert_bracks.join(", ")}"

    # Fix up nils so they don't get inserted as blank strings
    args = [] of YAML::Any | Nil
    data_inserts.flatten.each do |val|
      stricter_val = val.raw == nil ? nil : val
      args << stricter_val
    end

    db = DB.open(ENV["DATABASE_URL"]? || "postgres://postgres:@localhost:5432/slate_test")
    begin
      db.exec(query, args)
    ensure
      db.close
    end
  end
end
